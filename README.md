# ○班 複素数関係のライブラリの作成 #

### メンバー ###

* 33xx XXXXX
* 33yy YYYYY
* 33zz ZZZZZ

### 含まれるもの ###

* complex 関係の関数のためのテスト
* complex 関係の関数本体
* complex 関数を使った問題を解くプログラム
* 上記をコンパイルするための makefile

### 手順 ###

* make test でテストプログラムを作成し，実行する
* make でサンプルプログラムを作成し，実行する

### リポジトリサービスの利用例の説明文書 ###

* [moodle のブック](http://ee.metro-cit.ac.jp/moodle/mod/book/view.php?id=6222)
