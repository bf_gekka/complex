#ifndef TEST_H

#define TEST_H
#include <assert.h>
#include <math.h>
#include <stdlib.h>

#define message(m) fprintf(stderr,(m))
#define message1(m,a) fprintf(stderr,(m),(a))
#define message2(m,a,b) fprintf(stderr,(m),(a),(b))
#define message3(m,a,b,c) fprintf(stderr,(m),(a),(b),(c))
#define message4(m,a,b,c,d) fprintf(stderr,(m),(a),(b),(c),(d))
#define message6(m,a,b,c,d,e,f) fprintf(stderr,(m),(a),(b),(c),(d),(e),(f))
#define message8(m,a,b,c,d,e,f,g,h) fprintf(stderr,(m),(a),(b),(c),(d),(e),(f),(g),(h))
#define messend1(m,a) do{message1((m),(a));exit(1);}while(0)
#define messend2(m,a,b) do{message2((m),(a),(b));exit(1);}while(0)
#define messend3(m,a,b,c) do{message3((m),(a),(b),(c));exit(1);}while(0)
#define messend4(m,a,b,c,d) do{message4((m),(a),(b),(c),(d));exit(1);}while(0)
#define messend6(m,a,b,c,d,e,f) do{message6((m),(a),(b),(c),(d),(e),(f));exit(1);}while(0)
#define messend8(m,a,b,c,d,e,f,g,h) do{message8((m),(a),(b),(c),(d),(e),(f),(g),(h));exit(1);}while(0)
#define DELTA 1e-6

void testStart(char *mes);
void testEnd(void);
void assert_equals_int_func(int a, int b, char *fname, int line);
void assert_not_equals_int_func(int a, int b, char *fname, int line);
void assert_equals_float_func(float a, float b, char *fname, int line);
void assert_equals_double_func(double a, double b, char *fname, int line);
void assert_equals_mat22_func(double a[2][2], double b[2][2], char *fname, int line);

#define assert_equals_int(a, b) assert_equals_int_func(a, b, __FILE__, __LINE__)
#define assert_not_equals_int(a, b) assert_not_equals_func(a, b, __FILE__, __LINE)
#define assert_equals_float(a, b) assert_equals_float_func(a, b, __FILE__, __LINE__)
#define assert_equals_double(a, b) assert_equals_double_func(a, b, __FILE__, __LINE__)
#define assert_equals_mat22(a, b) assert_equals_mat22_func(a, b, __FILE__, __LINE__) 
#endif
